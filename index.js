const protobuf = require('protobufjs')
const SerialPort = require('serialport')
const bsp = require('bluetooth-serial-port')
const btSerial = new bsp.BluetoothSerialPort();

const root = protobuf.loadSync('messages.proto')
const Request = root.lookupType(`Request`)
const Response = root.lookupType(`Response`)


const devices = [
  '/dev/cu.AIY-2-Vision-2484-BtPro',
  '/dev/cu.AIY-2-Vision-2484-Seria',
  '/dev/tty.AIY-2-Vision-2484-BtPro',
  '/dev/tty.AIY-2-Vision-2484-Seria',
]

const request = Request.create({
  // getState: {},
  scanNetworks: {
    timeout: 10
  }
})
const encoded = Request.encode(request).finish()
console.log({encoded});

async function testDevice(device) {
  const port = new SerialPort(device)

  port.on('error', console.log);

  port.on('data', (data) => {
    console.log({data})
    const response = Response.decode(data)
    console.log({response})
  })

  port.on('open', (err) => {
    if (err) {
      console.log('open err', err);
      return;
    }
    console.log('Port open')

    port.write(encoded, () => {
      console.log('write complete')
      port.close();
    })
  })
}

devices.forEach(async device =>
  await testDevice(device)
);



